
from bliss.setup_globals import *

load_script("slits.py")

print("")
print("Welcome to your new 'slits' BLISS session !! ")
print("")
print("You can now customize your 'slits' session by changing files:")
print("   * /slits_setup.py ")
print("   * /slits.yml ")
print("   * /scripts/slits.py ")
print("")