
from bliss.setup_globals import *

load_script("pmacsimu.py")

print("")
print("Welcome to your new 'pmacsimu' BLISS session !! ")
print("")
print("You can now customize your 'pmacsimu' session by changing files:")
print("   * /pmacsimu_setup.py ")
print("   * /pmacsimu.yml ")
print("   * /scripts/pmacsimu.py ")
print("")