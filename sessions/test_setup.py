
from bliss.setup_globals import *

#load_script("test.py")

print("")
print("Welcome to your new 'test' BLISS session !! ")
print("")
print("You can now customize your 'test' session by changing files:")
print("   * /test_setup.py ")
print("   * /test.yml ")
print("   * /scripts/test.py ")
print("")