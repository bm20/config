
from bliss.setup_globals import *

load_script("testmot.py")

print("")
print("Welcome to your new 'testmot' BLISS session !! ")
print("")
print("You can now customize your 'testmot' session by changing files:")
print("   * /testmot_setup.py ")
print("   * /testmot.yml ")
print("   * /scripts/testmot.py ")
print("")