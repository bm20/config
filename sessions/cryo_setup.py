
from bliss.setup_globals import *
from bm20.misc.cryo.cryomv import cryomv

load_script("cryo.py")

print("")
print("Welcome to your new 'cryo' BLISS session !! ")
print("")
print("You can now customize your 'cryo' session by changing files:")
print("   * /cryo_setup.py ")
print("   * /cryo.yml ")
print("   * /scripts/cryo.py ")
print("")