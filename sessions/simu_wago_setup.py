
from bliss.setup_globals import *

load_script("simu_wago.py")

print("")
print("Welcome to your new 'simu_wago' BLISS session !! ")
print("")
print("You can now customize your 'simu_wago' session by changing files:")
print("   * /simu_wago_setup.py ")
print("   * /simu_wago.yml ")
print("   * /scripts/simu_wago.py ")
print("")