from bliss.common.logtools import user_warning
from bliss.setup_globals import *
import re

load_script("exafs.py")

print("")
print("Welcome to your new \"exafs\" BLISS session !! ")
print("")
print("You can now customize your \"exafs\" session by changing files:")
print("   * /exafs_setup.py ")
print("   * /exafs.yml ")
print("   * /scripts/exafs.py ")
print("")

ROI_NAME = "sca"

# if "sca" not in fx_ge18.rois:
#     # no rois, adding default rois
#     user_warning("\n\t=======================\n"
#                     "\t=======================\n"
#                     "\tNo ROI defined for the GE18 detector.\n"
#                     "\tAdding default ROIS ( = FULL RANGE).\n"
#                     "\tPlease:\n"
#                     "\t- RESTART the sessions,\n"
#                     "\t- then CHECK and EDIT the roi if necessary.\n"
#                     "\t=======================\n"
#                     "\t=======================\n")
#     fx_ge18.rois.set(ROI_NAME, *fx_ge18.spectrum_range)
#     input("Press Enter to continue...")
#     exit()


# chan_rx = re.compile("(.*)_det([0-9]{1,2})")

# aliases_dict = {"trigger_livetime": "lft",
#                 "realtime": "rlt"}

# def add_cnt(mg, cnt):
#     if not cnt.name.endswith("det6"):# and not cnt.name.endswith("det7"):
#         mg.add(cnt)
#         match = chan_rx.match(cnt.name)
#         if match:
#             cnt_name, chan = match.groups()
#             alias = aliases_dict.get(cnt_name, cnt_name)
#             ALIASES.add(f"{alias}{int(chan):02d}", cnt)

DEFAULT_CHAIN.set_settings(exafs_ge18_chain["chain_config"])
exafs_ge18_mg.set_active()
ge18.add_counters_to_measurement_grp(exafs_ge18_mg)
# groups = (
#     fx_ge18.counter_groups.spectrum,
#     fx_ge18.counter_groups.sca,
#     fx_ge18.counter_groups.icr,
#     fx_ge18.counter_groups.realtime,
#     fx_ge18.counter_groups.trigger_livetime)
# for group in groups:
#     for cnt in group:
        # add_cnt(exafs_ge18_mg, cnt)

from bm20.techniques.eh1_exafs.presets import *
preset = FastExafsPreset()
d20fscancfg.add_scan_preset(preset)

sg = femto
