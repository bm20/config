
from bliss.setup_globals import *

load_script("pilatus.py")

print("")
print("Welcome to your new 'pilatus' BLISS session !! ")
print("")
print("You can now customize your 'pilatus' session by changing files:")
print("   * /pilatus_setup.py ")
print("   * /pilatus.yml ")
print("   * /scripts/pilatus.py ")
print("")