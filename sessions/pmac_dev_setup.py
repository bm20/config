
from bliss.setup_globals import *
# from bm20.misc.tests.mono.counters import ecounter, scounter

load_script("pmac_dev.py")

print("")
print("Welcome to your new 'pmac_dev' BLISS session !! ")
print("")
print("You can now customize your 'pmac_dev' session by changing files:")
print("   * /pmac_dev_setup.py ")
print("   * /pmac_dev.yml ")
print("   * /scripts/pmac_dev.py ")
print("")

simu_mg.set_active()
# from bm20.utils.pmac import *
# from bm20.oh.mono.vbragg import pmac_cs_status_to_str
# from bm20.exafs.sequence.exafs_sequence import ExafsSequence

# from bm20.oh.d20monoxtal import get_xtal_axis
# xtal = get_xtal_axis()
# monoxtal._xtal._positioner = True

#debugon(pmacd20.pmac_comm)
#debugon('*_TurboPmac*')
#debugon(pmacd20)
#debugon(xtal1roll)

#from bliss.common.tango import DeviceProxy
#from bliss.common.standard import SoftAxis

#myint = 3

#dp = DeviceProxy('d20/limaccds/d20-cam3')

#sa1 = SoftAxis('bla', dp, position='image_height')
#sa2 = SoftAxis('sa2', dp, position='image_height')
#sa3 = SoftAxis('myint', dp, position='image_height')

#d20monocrystal._wago.set('error', 0)
#d20monocrystal._wago.set('homelat', 0)
#d20monocrystal._wago.set('homeyaw', 0)
