
from bliss.setup_globals import *
# from bm20.misc.tests.mono.motors import *
from fscan.musstscope import timescope 
# from bm20.scans.exafs import *

load_script("dev_fscan.py")

print("")
print("Welcome to your new 'dev_fscan' BLISS session !! ")
print("")
print("You can now customize your 'dev_fscan' session by changing files:")
print("   * /dev_fscan_setup.py ")
print("   * /dev_fscan.yml ")
print("   * /scripts/dev_fscan.py ")
print("")

# from bm20.misc.tests.fscan import d20fs
from bm20.sessions.xes5.scans import *

# for c in fx_ge18.counter_groups.realtime: ge18_mg.disable(c.name)
# for c in fx_ge18.counter_groups.triggers: ge18_mg.disable(c.name)
# for c in fx_ge18.counter_groups.spectrum: ge18_mg.disable(c.name)
# for c in fx_ge18.counter_groups.deadtime: ge18_mg.disable(c.name)
# for c in fx_ge18.counter_groups.energy_livetime: ge18_mg.disable(c.name)
# for c in fx_ge18.counter_groups.events: ge18_mg.disable(c.name)
# for c in fx_ge18.counter_groups.ocr: ge18_mg.disable(c.name)

# DEFAULT_CHAIN.set_settings(chain_exafs_dev["chain_config"])
#ge18_mg.set_active()
dev_fs_mg.set_active()