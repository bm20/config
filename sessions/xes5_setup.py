
from bliss.setup_globals import *


load_script("xes5.py")

print("")
print("Welcome to your new 'xes5' BLISS session !! ")
print("")
print("You can now customize your 'xes5' session by changing files:")
print("   * /xes5_setup.py ")
print("   * /xes5.yml ")
print("   * /scripts/xes5.py ")
print("")
#from bm20.sessions.xes5 import *


