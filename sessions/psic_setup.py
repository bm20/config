
from bliss.setup_globals import *

load_script("psic.py")

print("")
print("Welcome to your new 'psic' BLISS session !! ")
print("")
print("You can now customize your 'psic' session by changing files:")
print("   * /psic_setup.py ")
print("   * /psic.yml ")
print("   * /scripts/psic.py ")
print("")


def eiger500k_add():
    config.get("eiger500k")
    psic_mg.add(eiger500k.counters.image, eiger500k.counters.acq_time)

def eiger500k_remove():
    config.get("eiger500k")
    psic_mg.remove(*eiger500k.counters)

def pil100k_add():
    config.get("pil100k")
    psic_mg.add(pil100k.counters.image, pil100k.counters.acq_time)

def pil100k_remove():
    config.get("pil100k")
    psic_mg.remove(*pil100k.counters)


psic_mg.set_active()

try:
	eiger500k_add()
except:
	print("=========================")
	print("WARNING! EIGER 500k not set up.")
	print("=========================")
else:
	print(" Using EIGER 500k")
	
try:
	pil100k_add()
except:
	print("=========================")
	print("WARNING! PILATUS 100k not set up.")
	print("=========================")
else:
	print(" Using PILATUS 100k")
