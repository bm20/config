
from bliss.setup_globals import *
from bm20.scans.exafs import *

load_script("exafs_dev.py")

print("")
print("Welcome to your new 'exafs_dev' BLISS session !! ")
print("")
print("You can now customize your 'exafs_dev' session by changing files:")
print("   * /exafs_dev_setup.py ")
print("   * /exafs_dev.yml ")
print("   * /scripts/exafs_dev.py ")
print("")

DEFAULT_CHAIN.set_settings(exafs_dev_chain["chain_config"])
exafs_dev_mg.set_active()

# for c in fx_ge18.counter_groups.realtime: exafs_dev_mg.disable(c.name)
# for c in fx_ge18.counter_groups.triggers: exafs_dev_mg.disable(c.name)
# for c in fx_ge18.counter_groups.spectrum: exafs_dev_mg.disable(c.name)
# for c in fx_ge18.counter_groups.deadtime: exafs_dev_mg.disable(c.name)
# for c in fx_ge18.counter_groups.energy_livetime: exafs_dev_mg.disable(c.name)
# for c in fx_ge18.counter_groups.events: exafs_dev_mg.disable(c.name)
# for c in fx_ge18.counter_groups.ocr: exafs_dev_mg.disable(c.name)
