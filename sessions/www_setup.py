
from bliss.setup_globals import *

load_script("www.py")

print("")
print("Welcome to your new 'www' BLISS session !! ")
print("")
print("You can now customize your 'www' session by changing files:")
print("   * /www_setup.py ")
print("   * /www.yml ")
print("   * /scripts/www.py ")
print("")