
from bliss.setup_globals import *
# from bm20.misc.tests.mono.motors import *

from bm20.apps.elements.elements import get_binding_energy

load_script("xes_fast.py")

print("")
print("Welcome to your new 'xes_fast' BLISS session !! ")
print("")
print("You can now customize your 'xes_fast' session by changing files:")
print("   * /xes_fast_setup.py ")
print("   * /xes_fast.yml ")
print("   * /scripts/xes_fast.py ")
print("")


# if xes_has_roi():
#     xes_add_xiacnt_to_mg()
# else:
#     xes_set_roi(*xesm4.spectrum_range)


# def add_cnt(mg, cnt):
#     if cnt.name.endswith("det0"):
#         mg.add(cnt)

DEFAULT_CHAIN.set_settings(xes5_chain["chain_config"])

# if "sca" not in xesm4.rois:
#     xesm4.rois.set('sca', 0, 4095)
# xes5_mg.add(*xesm4.counter_groups.det0)

# ALIASES.add('ketek', xesm4.counters.sca_det0)
# ALIASES.add('xicr', xesm4.counters.icr_det0)
# ALIASES.add('xocr', xesm4.counters.ocr_det0)
# ALIASES.add('xlt', xesm4.counters.livetime_det0)
# ALIASES.add('xdt', xesm4.counters.deadtime_det0)

xes5_mg.set_active()
m4.add_counters_to_measurement_grp(xes5_mg)

# groups = (xesm4.counter_groups.spectrum,
#           xesm4.counter_groups.sca,
#           xesm4.counter_groups.ocr,
#           xesm4.counter_groups.icr,
#           xesm4.counter_groups.deadtime,
#           xesm4.counter_groups.trigger_livetime)
# for group in groups:
#     for cnt in group:
#         add_cnt(xes5_mg, cnt)

#from bm20.misc.tests.fscan import d20fs

from bm20.techniques.eh2_xes5.presets import *
preset = FastXes5Preset()
d20fscancfg.fscan.add_scan_preset(preset)
