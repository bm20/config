//////////////////////////////////////////////
//
// Multiplexeur Program for BM20 OPIOM
//
//////////////////////////////////////////////

// INPUT
wire ACQ_TRIG;
wire P201_GATEOUT;

// OUTPUT
reg XIA_START;

// ZAP Point or Spec Count
wire [1:0]SEL_ACQ_TRIG;

// Input/Output Assignment
assign ACQ_TRIG      = I1; // Input from OPIOM RACK O2 [ZAP Point Pulse or P201 GATEOUT]
assign P201_GATEOUT  = I2; // Input from P201 Gate Out

assign O1 = ACQ_TRIG;      // ACQ_TRIG to P201 Gate In
assign O2 = XIA_START;     // ACQ_TRIG to XIA [ZAP Point Pulse or SPEC Count]

// Register Assignement
assign SEL_ACQ_TRIG        = IM1;        // ZAP Point or Spec Count Selector

// source Shutter Control Selector
always @(SEL_ACQ_TRIG or ACQ_TRIG or P201_GATEOUT)
  begin
     case (SEL_ACQ_TRIG)
       2'b01 :   XIA_START = ACQ_TRIG;      // MUSST TRIGGER (ZAP)
       default : XIA_START = P201_GATEOUT;  // SPEC Count Gate
     endcase
  end

