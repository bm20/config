//////////////////////////////////////////////
//
// Multiplexeur Program for BM20 OPIOM
//
//////////////////////////////////////////////

// INPUT
wire MUSST_TRIG;
wire P201_GATEOUT;

// OUTPUT
reg P201_TRIG;
reg XIA_START0;
reg XIA_START1;
reg XIA_START2;
reg XIA_START3;
reg XIA_START4;
reg MISC_OUT;

// ZAP Point or Spec Count
wire [1:0]SEL_TRIG;

// Input/Output Assignment
assign MUSST_TRIG      = I1; // Input from OPIOM RACK O2 [ZAP Point Pulse or P201 GATEOUT]
assign P201_GATEOUT  = I2; // Input from P201 Gate Out

assign O1 = MUSST_TRIG;      // MUSST_TRIG to P201 Gate In
assign O2 = XIA_START0;     // MUSST_TRIG to XIA [ZAP Point Pulse or SPEC Count]
assign O3 = XIA_START1;     // MUSST_TRIG to XIA [ZAP Point Pulse or SPEC Count]
assign O4 = XIA_START2;     // MUSST_TRIG to XIA [ZAP Point Pulse or SPEC Count]
assign O5 = XIA_START3;     // MUSST_TRIG to XIA [ZAP Point Pulse or SPEC Count]
assign O6 = XIA_START4;     // MUSST_TRIG to XIA [ZAP Point Pulse or SPEC Count]
assign O7 = MISC_OUT;     

// Register Assignement
assign SEL_TRIG        = IM1;        // ZAP Point or Spec Count Selector

// source Shutter Control Selector
always @(SEL_TRIG or MUSST_TRIG or P201_GATEOUT)
  begin
     case (SEL_TRIG)
       2'b01 :   begin
                   P201_TRIG = MUSST_TRIG;      // P201 trigged by musst
                   XIA_START0 = MUSST_TRIG;      // MUSST TRIGGER (ZAP)
                   XIA_START1 = MUSST_TRIG;      // MUSST TRIGGER (ZAP)
                   XIA_START2 = MUSST_TRIG;      // MUSST TRIGGER (ZAP)
                   XIA_START3 = MUSST_TRIG;      // MUSST TRIGGER (ZAP)
                   XIA_START4 = MUSST_TRIG;      // MUSST TRIGGER (ZAP)
                   MISC_OUT = MUSST_TRIG;
                 end
       default : begin
                   P201_TRIG = 0;              // P201 internal trigger
                   XIA_START0 = P201_GATEOUT;  // SPEC Count Gate
                   XIA_START1 = P201_GATEOUT;  // SPEC Count Gate
                   XIA_START2 = P201_GATEOUT;  // SPEC Count Gate
                   XIA_START3 = P201_GATEOUT;  // SPEC Count Gate
                   XIA_START4 = P201_GATEOUT;  // SPEC Count Gate
                   MISC_OUT = P201_GATEOUT;
                 end
     endcase
  end
