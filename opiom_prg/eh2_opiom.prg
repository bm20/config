// OPIOM (Octal Programmable I/O Module)
//
// Test to see how to blink O2 and O3 when I1 is triggered from P201
wire P201GATE;

// The P201GATE is coming in on the first input

assign P201GATE = I1;

// Front panel outputs O2 and O3 will be having the same pulse
assign O2 = P201GATE;
assign O3 = P201GATE;

// End of default user code
